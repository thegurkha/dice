/*
 * dp.c
 *
 * Copyright 2019 Dave McKay - dave@interestingpoint.co.uk
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include <sqlite3.h>

sqlite3 *WordsDB = NULL;

void WordQuery(char *pSearchClue );

int main( int agrc, char *argv[] )
{
  int nResponse;
  char strQuery[ 7 ];

  // seed random number routines
  srand( ( unsigned ) time( NULL ) % 37 );

  char szDatabaseName[]="dice_words.sl3";	// name of the words database

  // intialise the SQLite library
	sqlite3_initialize();

	// open the database
	nResponse = sqlite3_open_v2(szDatabaseName, &WordsDB, SQLITE_OPEN_READWRITE, NULL);

	// if it didn't open OK, shutdown and exit
	if (nResponse != SQLITE_OK) {

		printf("Couldn't open words database.\n" );

		sqlite3_close(WordsDB);
		exit (-1);
	}

  for ( int nWordLoop = 0; nWordLoop < 3; nWordLoop++ ) {

    for ( int i = 0; i < 5; i++ ) {

      strQuery[ i ] = ( rand() % 6 ) + 1 + 48;
    }

    strQuery[ 5 ] = 0x00;

    WordQuery( strQuery );

    printf("%c", ( nWordLoop < 2 ) ? '.' : '\n' );
  }

  // shut down sqlite3
  sqlite3_close(WordsDB);
  sqlite3_shutdown();

  exit ( 0 );

} // end of main

void WordQuery(char *pSearchClue )
{
	char szTemplate[]="SELECT * FROM words WHERE word_id=\"%s\"";
	char szSQLString[100]="";
	char *pRetrievedWord;
	int nResponse;
	sqlite3_stmt *stmt = NULL;

	// create the SQL string
	sprintf(szSQLString, szTemplate, pSearchClue);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(WordsDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	while (nResponse == SQLITE_ROW) {

		// take a copy of the three elements of the fact
		pRetrievedWord = (char*) sqlite3_column_text( stmt, 1 );

		// check for the next record
		nResponse = sqlite3_step(stmt);
	}

  printf( "%s", pRetrievedWord );

	// close off the sql statement
	sqlite3_finalize(stmt);

}	// end of WordQuery
