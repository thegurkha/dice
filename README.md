# README #

Simple program to generate passphrases using the Diceware system.
https://en.wikipedia.org/wiki/Diceware

### What is this repository for? ###

Simple implementation of Diceware passphrase generation system.

### How do I get set up? ###

Check out the source and make.

### Contribution guidelines ###

Get in touch!

### Who do I talk to? ###

Dave McKay
dave@interestingpoint.co.uk